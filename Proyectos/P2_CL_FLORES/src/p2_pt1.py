
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import seaborn as sns

#1. Importar conjunto de datos de Iris
data_frame = pd.read_csv("Iris.csv")
print(data_frame.head())
n_col = len(data_frame.columns)
n_rows = len(data_frame.index)

print("\nNúmero de columnas = %i" % n_col)
print("Número de filas = %i\n" % n_rows)

# 2. Matriz de correlación 
""" 
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure

cols = data_frame.columns.to_numpy()
correlation_matrix = data_frame.corr().round(2)
fig, ax = plt.subplots(figsize=(6,6))
sns.set(font_scale=0.7)
hm = sns.heatmap(correlation_matrix, annot=True)
plt.show()
 """
#3. scatter plots

import matplotlib.pyplot as pltlos
from matplotlib.pyplot import figure
import seaborn as sns

sns.set(style='whitegrid', context='notebook')
cols = data_frame.columns


iset_dt = data_frame[data_frame["Species"] == "Iris-setosa"]
iversc_dt = data_frame[data_frame["Species"] == "Iris-versicolor"]
ivirg_dt = data_frame[data_frame["Species"] == "Iris-virginica"]

figure(figsize=(10, 3), dpi=50)
sns.pairplot(iset_dt[cols], height=2.5, hue="species");
plt.show()
 
""" feature = 'PetalLengthCm'
target = data_frame['PetalWidthCm']

plt.figure(figsize=(20, 5))
plt.subplot(1, 1 , 1)
x = data_frame[feature]
y = target
plt.scatter(x, y, marker='o', color='r')
plt.title(feature)
plt.xlabel(feature)
plt.ylabel('PetalWidthCm')
plt.show() """

""" 
Podemos observar que en este conjunto de datos tenemos, cuatro propiedades importantes de las flores:
la longitud y ancho del sepalo de la flor, y la longitud y ancho del petalo de la flor.
"""






# Datos de entrenamiento y prueba 30-70

# Datos de entrenamiento y prueba 50-50

# Datos de entrenamiento y prueba 70-30

""" 
def sigmoid(z):
    return 1.0/(1.0+np.exp(-z))

z = np.arange(-7,7, 0.1)
phi_z = sigmoid(z)

plt.plot(z,phi_z,'r')
plt.xlabel('z')
plt.ylabel('$\phi (z)$')
plt.ylim([-0.1,1.1])
plt.yticks([0.0, 0.5, 1.0])
plt.grid()
plt.show() """