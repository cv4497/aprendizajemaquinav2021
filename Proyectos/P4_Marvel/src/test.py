import pandas as pd

train_df = (pd.read_csv('notebook/marvel_train.csv')).drop(index = [0,1])
test_df = (pd.read_csv('notebook/marvel_test.csv')).drop(index = [0,1])

Marvel = pd.concat([test_df, train_df])
columns, rows = Marvel.shape


print(Marvel)
y = Marvel["category"]
X = Marvel.drop(columns = ["category", "image name", "image", "size", "width", "height"])


from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split

train_percentage = 70
test_sz = round(1 - (train_percentage/100), 2)

# separación de datos 70% entramiento - 30 prueba%
X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size = test_sz, random_state=5)
print(X_train.shape)


from sklearn.ensemble import RandomForestClassifier 

model_100_rf = RandomForestClassifier(n_estimators=100)

import time
start_time = time.time()
model_100_rf.fit(X_train, Y_train)
print("--- %s seconds ---" % (time.time() - start_time))

import numpy as np
# prueba del modelo
y_100_rf_predict = model_100_rf.predict(X_test)
print(np.array(Y_test[:7]))
print(y_100_rf_predict[:7])